## Topics Covered

The main goal of this workshop is to motivate and nourish best practices at any stage of the teaching process. For this, we would like to cover a structured approach to teaching motivated by [the carpentries][cdh] or a variation thereof. As we believe that core concepts contained in this are vital for any teaching practitioners, we will discuss these during the workshop. Aspects of these include: course context, pre- and post-workshop surveys, learner profiles, learning goals and objectives, giving teaching feedback. With this, we hope to equip attendees with a structured approach to teaching.

The central activity of the workshop will be a (potentially parallel) **presentation of 5-10 minute lightning talks** in the afternoon session. These contributions recruit themselves from a **call-for-papers prior to the workshop**. We like to attract at maximum 3 page long mini articles that present or discuss a teaching activity related to machine learning. These mini papers are expected to present teaching examples from various aspects of teaching ML. For example:

* a demo of how to teach backpropagation
* expectation management for non-computer science learners of ML
* a discussion of an instructive data set for teaching Convolutional Neural Networks
* an interactive web application to play with parameters of a classifier (SVM, CNN, MLP, ...)
* a teaching metaphor to illustrate time series prediction
* a (interactive) vizualisation of stochastic gradient decent

Depending on the interest in the workshop, we will conduct an open peer review on all contributions and select contributions based on the reviewers feedback.

Participants of the afternoon session will be motivated to provide feedback to their peers. Depending on the room and number of submissions, we will divide the presentations based on the field they focus on: vision applications, language applications, general concepts etc. Each of these working groups is asked to collect general patterns on what works and what doesn't. After this session, we will compile a report to summarize and publish the findings of this event and to lay the foundation for furture activites.
