## Authors

### Peter Steinbach

**contact**: [p.steinbach@hzdr.de](mailto:p.steinbach@hzdr.de), [twitter:psteinb_](https://twitter.com/psteinb_)

**institute**: [HZDR](https://hzdr.de)

**job**: Team Lead AI Consultants for Matter Research

Peter received his PhD in Particle Physics in 2012 from the [TU Dresden](https://tu-dresden.de) for an experimental study of LHC data using the ATLAS experiment to reduce background contributions to Higgs Particle searches. He continued to industry as a HPC support and software engineer helping scientists push the limits of their applications in a service oriented group. In this role, he become increasingly exposed to Deep Learning applications for vision applications in biology. In 2019, he started to lead a group of AI consultants that aims to help scientists from the research field matter at [Helmholtz society](https://www.helmholtz.de/) to use machine learning in experiment and theory.


### Oliver Guhr

**contact**: [oliver.guhr@htw-dresden.de](mailto:oliver.guhr@htw-dresden.de), [twitter:oliverguhr](https://twitter.com/oliverguhr)

**institute**: Department of Artificial Intelligence, [HTW University of Applied Sciences Dresden](https://www.htw-dresden.de/en/)

**job**: Research Fellow

Oliver received a bachelor degree in business informatics in 2014 (HfT Leipzig) and holds a master degree in computer science since 2018 (HTW Dresden). From 2007 to 2018 he was working as a software engineer in the sector of information and communication technology. In 2018 he became a research fellow at the HTW Dresden in the department of artificial intelligence.
His research focuses on Spoken Dialogue Systems, Machine Learning, and Natural Language Processing. He also teaches the Natural Language Processing part of the Deep Learning course at HTW Dresden.
Oliver took part in the organization of several conferences. From 2015 to 2017 he was part of the team that organized "[DevDay](https://www.devday.de/)" a practitioners conference on software development. Since 2018 he is part of a team that organizes "[MobileCamp](https://mobilecamp.de/)" a Barcamp on mobile computing. In 2019 he was part of the project team that organized an international summer school on "[Voice Interaction and Voice Assistants in Health Care](https://viva2019.de)".


### Heidi Seibold

**contact**: [heidi.seibold@lmu.de](mailto:heidi.seibold@lmu.de), [twitter:HeidiBaya](https://twitter.com/HeidiBaya)

**institute**: [LMU Munich](https://www.compstat.statistik.uni-muenchen.de/), [Bielefeld University](http://www.wiwi.uni-bielefeld.de/lehrbereiche/statoekoinf/dasc/), [Helmholtz Zentrum München](https://www.helmholtz-muenchen.de/icb/research/groups/fuchs-lab/overview/index.html)

**job**: Postdoc / Open Science Advocate

Heidi's research deals with open and reproducible science and with machine learning methods for personalized medicine. Heidi finished her PhD in Statistics in 2018 at the University of Zurich, where she also received a teaching certificate. After her PhD she took on a position as group lead of the [DIFUTURE](https://difuture.de/) analysis group at LMU Munich. In 2019 she was visiting professor at the [statistics department at LMU Munich](https://www.statistik.uni-muenchen.de/index.html), where she now continues working part time as postdoc in the Computational Statistics group. The rest of her time she works in the Data Science group at Bielefeld University with working location at Helmholtz Zentrum München. Most of her time she spends working in the field of Open Science. She is an Open Scholarship expert for [Knowledge Exchange](http://www.knowledge-exchange.info/) and a member of the [LMU Open Science Center](https://www.osc.uni-muenchen.de/index.html). Her teaching focuses on statistical methods, R programming, and machine learning. She has experience in unconventional teaching methods including flipped-classroom courses and problem-based learning.


### Bernd Bischl

**contact**: [bernd.bischl@stat.uni-muenchen.de>](mailto:bernd.bischl@stat.uni-muenchen.de), [twitter:BBischl](https://twitter.com/BBischl)

**institute**: [LMU Munich](https://www.compstat.statistik.uni-muenchen.de/), [Munich Center for Machine Learning](https://mcml.ai)

**job**: Full Professor

Bernd holds the chair of Statistical Learning and Data Science at the LMU, Munich. He conducts broad research in the field of machine learning, especially in AutoML and interpretable machine learning. Furthermore, he is a director of one of Germanies newly  stablished AI competence center "Munich Center for Machine Learning" (MCML). Bernd teaches Machine Learning, Deep Learning and Statistical Computing in the bachelor and master programs for statistics and data science at LMU. He is also the founder of the open source R toolbox mlr and co-founder of the OpenML platform. 


## Supporters

Amir Farbin, afarbin@cern.ch, Associate Professor University of Texas at Arlington  
 
Florian Huber, f.huber@esciencecenter.nl, Data Scientist/eScience Research Engineer, Netherlands eScience Center, Amsterdam, NL
