## Appeal to ICML audiences

Many experts and practitioners who develop Machine Learning models or infrastructure around these models are confronted with the opportunity or duty to teach machine learning at some point in their career. Traditionally, many  rely on their gut feeling to design courses that are motivated by these circumstances. The methods of choice are often Power Point or similar technologies and a lot of copy&pasting from the web. 

This workshop targets those who would like to know how teachers from around the globe approach teaching Machine Learning: How deep do they dive into the matter? What mental models do they use to visualize concepts? What media is at play in teaching ML by others? 

With this workshop, we hope that by the end of the day, all participants have a better feeling where they stand with their teaching. By collecting teaching examples, we also hope to lay the basis of subsequent studies on trends and directions in the field of didactics of AI.


