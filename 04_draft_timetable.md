## Tentative Timetable

| time     | Title                                          | Speaker             |
| --:      | :--                                            | :--                 |
| 9.00 am  | Welcome                                        | Organizers          |
| 9.30 am  | Didactics of Data                              | [Rebecca Fiebrink](https://www.doc.gold.ac.uk/~mas01rf/homepage/)    |
| 10.00 am | Experiences with MOOCs                         | [Elisabeth Sulmont](https://www.datacamp.com/instructors/lis-7740a67d-ab2a-4517-a218-5ad9fd6cb998) |
| 10.30 am | Coffee break                                   |                     |
| 11.00 am | Experiences in Lectures                        | [Heide Seibold](http://www.compstat.statistik.uni-muenchen.de/people/seibold/), [Bernd Bischl](https://www.compstat.statistik.uni-muenchen.de/people/bischl/) |
| 11.30 am | Experiences in Bootcamps/Compact Courses       | [Anne Fouillioux](https://www.mn.uio.no/geo/english/people/adm/annefou/), [Peter Steinbach](https://github.com/psteinb/) |
| 12.00 am | Lunch                                          |                     |
| 1.00 pm  | Preface (Parallel) Teaching Example Session    | Organizers          |
| 1.30 pm  | How to give feedback                           | Organizers          |
| 2.15 pm  | Teaching Example Presentations                 | All                 |
| 2.45 pm  | Coffee break                                   |                     |
| 3.15 pm  | Teaching Example Presentations                 | All                 |
| 5.00 pm  | Coffee break                                   |                     |
| 5.30 pm  | Summary Teaching Example Presentations         | All                 |
| 6.00 pm  | Farewell and Next Steps                        | Organizers          |
| 6.30 pm  | End                                            |                     |
